
![](https://img.shields.io/badge/Microverse-blueviolet)

# Umuhanzi-Concert-Website
This is the website for music festival or concert tour where the visitors will get the information about the schedule on which the festivals will be taking place and the location where they will be held. They will be able to know the host and the featured artists who will be performing and also information about the organisers.
Moreover, there is the opportinity to stream live the concerts and also the portal for buying and booking the tickets.

This website may help all concert organisers to reach out to their customers in an easy way using this online platform.

## Built With

- HTML
- CSS
- Javascript

## How to use and run this project

>To get a local copy up and running follow these simple example steps.

-Clone this repository with

git clone https://github.com/BertrandConxy/umuhanzi-concert-website.git using your terminal or command line.

-Change to the project directory by entering :

cd umuhanzi-concert-website in the terminal.

-run code . to open it in vscode


It is a website and It can be opened in any browser.

It is a mobile-first layout which is also responsive to different screens including desktops and laptops.

I have put a live demo link below in the following section so that
if anyone is interested can view it. However, if anyone would like to use it, simply, they can fork the repository and then use it independently.
There are no other dependencies or installations required.

## Project Status
This project is still under development.

## Screenshoot
![Screenshot (97)](https://user-images.githubusercontent.com/90222110/149523231-d7b039e0-7f16-4174-b6f4-be8c5e7608d0.png)

![Screenshot (99)](https://user-images.githubusercontent.com/90222110/149523267-91ca2dc2-d95d-4b6c-adb0-fb814164cab6.png)
![Screenshot (100)](https://user-images.githubusercontent.com/90222110/149523277-acae3ea8-b1e4-45e0-a73c-712fc039f088.png)
![umuhanzi-mobile-1](https://user-images.githubusercontent.com/90222110/149523304-91d4f401-8fe4-4627-a825-ae27cfecdf04.jpg)
![umuhanzi-mobile-5](https://user-images.githubusercontent.com/90222110/149523319-ff6d8a22-4a04-4c78-9914-1148134325f7.jpg)

## Live Demo link
 https://bertrandconxy.github.io/umuhanzi-concert-website/

## Issues

Up to now, there are no issues with it.

Here is the link to the Issues tab:

https://github.com/BertrandConxy/umuhanzi-concert-website/issues

## Authors

👤 **Bertrand Mutangana Ishimwe**

- GitHub: [@BertrandConxy](https://github.com/BertrandConxy)
- Twitter: [@Bconxy](https://twitter.com/Bconxy)
- LinkedIn: [Bertrand Mutangana Ishimwe](https://www.linkedin.com/in/bertrand-mutangana-024905220/)


## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Thanks to Cindy Shin whose design of cc global summit 2015 was used.
- Thanks to other people who helped me finish this project.

